<?php

/**
 * Class ProductTest
 */
class ProductTest extends \Model_TestCase
{
    /**
     * test get_water_info_by_contract_id
     * @dataProvider get_water_info_by_contract_id_data_provider
     * [CRE] - [tien_dat] - [2018-07-13]
     */
    public function test_get_water_info_by_contract_id($input, $expected)
    {
        $this->debug = false;
        $result = $this->CI->product->get_water_info_by_contract_id($input['contract_id']);
        $this->assertEquals($result, $expected);
    }

    public function get_water_info_by_contract_id_data_provider ()
    {
        return $this->getDataProvider($this->allocation);
    }
}