<?php

/**
 * Class RegistrationTest
 */
class RegistrationTest extends \Ci_Framework_TestCase
{
    protected $allocation = 'widgets/_staff';
    /**
     * test get_water_info_by_contract_id id
     * @dataProvider demo_data_provider
     */
    public function test_demo($input, $expected)
    {
        $this->assertEquals($expected, true);
    }

    public function demo_data_provider ()
    {
        return $this->getDataProvider($this->allocation);
    }
}