<?php

/**
 * Class CalDeliveryHelperTest
 */
class CalDeliveryHelperTest extends \Ci_Framework_TestCase
{
    protected $allocation = 'helpers';
    /**
     * test get_delivery_date_scope
     * @dataProvider get_delivery_date_scope_data_provider
     * [CRE] - [tien_dat] - [2018-07-13]
     */
    public function test_get_delivery_date_scope($input, $expected)
    {
        $this->CI->load->helper('cal_delivery_helper');

        //load lang by specific module
        $this->CI->lang->load($input['file_name'], '', false, true, '', $input['module']);

        $result = get_delivery_date_scope($input['nearest_day'], $input['period_time']);
        $this->assertEquals($expected, $result);
    }

    /**
     * Data test function get_delivery_date_scope
     * @return array|bool
     */
    public function get_delivery_date_scope_data_provider()
    {
        return $this->getDataProvider($this->allocation);
    }
}