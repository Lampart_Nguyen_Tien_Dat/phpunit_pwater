<?php
/**
 * Created by PhpStorm.
 * User: bao_trung
 * Date: 6/13/2018
 * Time: 10:09 AM
 */
//require_once __DIR__ . '/../../utils/FakeRequest.php';
//include_once('/HttpRequest.php');
class AuthenticationTest extends Model_TestCase {
    protected $debug = false;

    /**
     * test initAppMenuDynamic
     * @dataProvider initAppMenuDynamicDataProvider
     */
    public function testInitAppMenuDynamic($input, $expected){
        $url = $input['url'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);


        $result = curl_exec($ch);
        if(curl_errno($ch) !== 0) {
            error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }

        curl_close($ch);
        $result = json_decode($result, true);
        $this->assertEquals($expected, $result['status']);
    }

    public function initAppMenuDynamicDataProvider(){
        return array(
            'premium_water' => array(
                array(
                    'url' => 'http://dev.lampart.com.vn/mypage/authenticate/initAppMenuDynamic/request_type/1/certificate/bi9VRTlNdkUzeXZDdm9OMngzWjVQNytsc0ptaEUwSnZLUEx6QlJkeHVnMTlrZis5TjRNaElZWW5SYTNwMWhleEZYd0NpZTJlc1ZQd0lhemlDMVExZHc9PQ',
//                    'url' => 'https://dev-pw-jp.fraise.jp/mypage/authenticate/initAppMenuDynamic/request_type/1/certificate/aU5TZC9aUXFhaHVOY2w4eE03Ly9kQ0hwb1pYQ1h0RW8rTDBualBoM28rMWk3VExsTkQ4eCswNlV1emdzWC84aCtoZ1dWWWt3Vm81RjQxTHZXSzBpSVE9PQ',
                ),
                'status' => '1'
            ),
            'false' => array(
                array(
                    'url' => 'http://dev.lampart.com.vn/mypage/authenticate/initAppMenuDynamic/request_type/1/certificate/bi9VRTlNdkUzeXZDdm9OMngzWjVQNytsc0ptaEUwSnZLUEx6QlJkeHVnMTlrZis5TjRNaElZWW5SYTNwMWhleEZYd0NpZTJlc1ZQd0lhemlDMVExZHc9PQ1',
//                    'url' => 'https://dev-pw-jp.fraise.jp/mypage/authenticate/initAppMenuDynamic/request_type/1/certificate/aU5TZC9aUXFhaHVOY2w4eE03Ly9kQ0hwb1pYQ1h0RW8rTDBualBoM28rMWk3VExsTkQ4eCswNlV1emdzWC84aCtoZ1dWWWt3Vm81RjQxTHZXSzBpSVE9PQ1',
                ),
                'status' => '0'
            )
        );
    }
}