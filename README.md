####################
# We special thank to Mr. Tuan Dung. We build this project follow his project at https://github.com/SpartaTeamDev/pwater_tests.
# So many thanks.
####################


# Setup
####################

# Setup PHPUnit
####################
# Clone Git repository phpunit_pwater to www folder (this directory include pwater folder):
  git clone https://Lampart_Nguyen_Tien_Dat@bitbucket.org/Lampart_Nguyen_Tien_Dat/phpunit_pwater.git
# Copy files setup/application/*.* to CodeIgniter [v2.1.3 only] application folder, e.g. D:/A20P52/www/pwater/application
# Copy files setup/system/*.* to CodeIgniter [v2.1.3 only] system folder, e.g. D:/A20P52/www/pwater/system
# Install composer into virtual box if not exist.
  composer: https://www.epictrim.com/ideas/install-composer-on-centos/  
# Go into phpunit_pwater folder and run the following command: composer install

# Read more: https://phpunit.de/manual/4.8/en/index.html


# Run PHPUnit
####################
# In virtual box, change dir to "/media/sf_www/phpunit_pwater".
# Then run this command: "/media/sf_www/phpunit_pwater/vendor/bin/phpunit -c /media/sf_www/phpunit_pwater/pwater.xml"

# More document in  http://pwater-blog.com/phpunit-for-pwater/