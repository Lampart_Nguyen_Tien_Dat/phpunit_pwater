<?php

if (isset($_REQUEST['folder'])) {
    $files = scandir('../test_data/' . $_REQUEST['folder']);
    array_shift($files);
    array_shift($files);
    header('Content-type: application/json');
    echo json_encode($files);
}

if (isset($_REQUEST['file_name'])) {
    $files = scandir('../test_data/' . $_REQUEST['allocation']);

    if(in_array($_REQUEST['file_name'], $files)){
        unlink('../test_data/' . $_REQUEST['allocation'].'/'.$_REQUEST['file_name']);
    }

    $file = fopen('../test_data/' . $_REQUEST['allocation'].'/'.$_REQUEST['file_name'],"w");
    fwrite($file, $_REQUEST['data']);
    fclose($file);

    header('Content-type: application/json');
    echo json_encode(array('file_name' => $_REQUEST['file_name'], 'folder' => $_REQUEST['allocation']));
}

if (isset($_REQUEST['run_test'])) {
    if(!empty($_REQUEST['cmd'])){
        exec("/media/sf_www/phpunit_pwater/vendor/bin/phpunit --filter ".$_REQUEST['cmd'].' -c /media/sf_www/phpunit_pwater/pwater.xml');
    }else{
        exec("/media/sf_www/phpunit_pwater/vendor/bin/phpunit -c /media/sf_www/phpunit_pwater/pwater.xml");
    }
}

if (isset($_REQUEST['get_dir'])) {
    $dir = scandir('../test_data/');
    array_shift($dir);
    array_shift($dir);

    header('Content-type: application/json');
    echo json_encode($dir);
}