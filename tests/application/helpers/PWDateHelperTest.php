<?php

/**
 * Class PWDateHelperTest
 */
class PWDateHelperTest extends \Ci_Framework_TestCase
{
    protected $allocation = 'helpers';
    /**
     * test geteto
     * @dataProvider geteto_data_provider
     * [CRE] - [tien_dat] - [2018-07-13]
     */
    public function test_geteto($input, $expected)
    {
        $this->CI->load->helper('date_helper');
        $result = geteto($input['year']);
        $this->assertEquals($expected, $result);
    }

    public function geteto_data_provider()
    {
        return $this->getDataProvider($this->allocation);
    }
}