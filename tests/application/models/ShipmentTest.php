<?php

/**
 * Class ShipmentTest
 */
class ShipmentTest extends \Model_TestCase
{
    /**
     * test get_shipment_location_by_conditions
     * @dataProvider get_shipment_location_by_conditions_data_provider
     * [CRE] - [tien_dat] - [2018-07-20]
     * Test for task #35107
     */
    public function test_get_shipment_location_by_conditions($input, $expected)
    {
        $this->debug = false;
        $result = $this->CI->shipment->get_shipment_location_by_conditions(
            $input['order_type'],
            $input['water_company_id'],
            $input['delivery_company_id'],
            $input['partner_id'],
            $input['area_id'],
            $input['plus_params']
        );
        $this->assertEquals($expected, $result);
    }


    public function get_shipment_location_by_conditions_data_provider ()
    {
        return $this->getDataProvider($this->allocation);
    }
}