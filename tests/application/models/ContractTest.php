<?php

/**
 * Class ContractTest
 */
class ContractTest extends \Model_TestCase
{
    protected $allocation = 'models';
    /**
     * test is_apply_bonds_net_for_order_data
     * @dataProvider is_apply_bonds_net_for_order_data_data_provider
     * [CRE] - [tien_dat] - [2018-07-13]
     */
    public function test_is_apply_bonds_net_for_order_data($input, $expected)
    {
        $result = $this->CI->contract->is_apply_bonds_net_for_order_data(
            $input['contract_id'],
            $input['is_order_first'],
            $input['override_param']
        );
        $this->assertEquals($expected, $result);
    }

    public function is_apply_bonds_net_for_order_data_data_provider ()
    {
        return $this->getDataProvider($this->allocation);
    }

    /**
     * test rollback data
     * @dataProvider demo_rollback_data_data_provider
     */
    public function test_demo_rollback_data($input, $expected) {
        $contract_params[] = $input;
        $this->CI->contract->update_batch_contract($contract_params);
        $query = "SELECT ". implode(', ', array_keys($contract_params[0])) ." FROM contract where id = ". $contract_params[0]['id'];
        $a = $this->CI->db->query($query);
        $arr = $a->row_array();
        $this->assertEquals($expected,$arr);
    }

    public function demo_rollback_data_data_provider(){
        return $this->getDataProvider($this->allocation);
    }
}