<?php

/**
 * Class ZipcodeTest
 */
class ZipcodeTest extends \Model_TestCase
{
    /**
     * test check_zipcode_delivery_company_allocation_data_exist
     * @dataProvider check_zipcode_delivery_company_allocation_data_exist_data_provider
     * [CRE] - [tien_dat] - [2018-07-13]
     */
    public function test_check_zipcode_delivery_company_allocation_data_exist($input, $expected)
    {
        $this->debug = false;
        $result = $this->CI->zipcode->check_zipcode_delivery_company_allocation_data_exist(
            $input['zipcode'],
            $input['delivery_company_id'],
            $input['water_company_id']
        );
        $this->assertEquals($expected, $result);
    }

    public function check_zipcode_delivery_company_allocation_data_exist_data_provider ()
    {
        return $this->getDataProvider($this->allocation);
    }
}