$(document).ready(function(){
    get_dir();

    $('#get_file_content').click(function () {
        var allocation = $('#allocation').val();
        var file_name  = $('#files').val();
        $.getJSON("../test_data/" + allocation + '/' + file_name, function (result){
            if(jQuery.isEmptyObject(result) === false){
                $('#show_data').empty();
                $('#show_data').append("<textarea id='text'>" +  JSON.stringify(result, null, 2) + "</textarea >");
                var copyText = document.getElementById("text");
                copyText.select();
                document.execCommand("copy");
            }
        });
    });

    $('#allocation, #widgets_child').change(function () {
        $('#files').empty();
        get_files($('#allocation').val());
    });

    $('#files').change(function () {
        process_class_and_func_name($(this).val());
    });

    $('#create_file').click(function () {
        $('#show_data').empty();
        $('#class_name, #function_name').val('');
        $('#show_data').append("<textarea id='text'></textarea>");
    });
    
    $('#save_file').click(function () {
        var function_name = $('#function_name').val();
        var class_name    = $('#class_name').val();
        var allocation    = $('#allocation').val();
        var data          = $('#text').val();
        var widgets_child = $('#widgets_child').val();
        $('#msg').empty();
        if(function_name.length <= 0){
            $('#msg').append('Function namne is empty');
            return;
        }
        if((class_name.length <= 0) && (allocation !== 'helpers')){
            $('#msg').append('Class namne is empty');
            return;
        }
        var file_name = class_name + ' - '  + function_name;
        allocation = allocation === 'widgets' ? (allocation + '/' + widgets_child) : allocation;
        $.ajax({
            url: 'common.php',
            data: {
                'file_name'  : file_name,
                'allocation' : allocation,
                'data'       : data
            },
            success: function (result) {
                $('#msg').append('Added file ' + result.file_name + ' to folder ' + result.folder);
            }
        });
    });

    //get file result and render
    if($('#result').val() ===  ''){
        $.ajax({
            url: "../log/result.xml" ,
            dataType: "xml" ,
            success: function(xml) {
                var result = {};
                var curr_allo  = 'filter';
                var curr_class = '';
                var curr_func  = '';

                //prepare data
                $(xml).find("testsuite").each(function(){
                    if($(this).attr('name').indexOf('Test::') !== -1){
                        curr_func = $(this).attr('name').replace(curr_class + 'Test::test_', '');
                        result[curr_allo][curr_class][curr_func] = {
                            "attr" : {
                                "tests"      : $(this).attr('tests'),
                                "assertions" : $(this).attr('assertions'),
                                "failures"   : $(this).attr('failures'),
                                "errors"     : $(this).attr('errors'),
                                "time"       : process_time($(this).attr('time'))
                            }
                        };

                        $(xml).find("testcase").each(function(){
                            if($(this).attr('name').indexOf(curr_func) !== -1){
                                var test_case_name = $(this).attr('name').substring($(this).attr('name').indexOf('"') + 1, $(this).attr('name').length - 1);

                                result[curr_allo][curr_class][curr_func][test_case_name] = {
                                    "assertions" : $(this).attr('assertions'),
                                    "time"       : process_time($(this).attr('time'))
                                };
                                if($(this).find("failure").length !== 0){
                                    result[curr_allo][curr_class][curr_func][test_case_name]['failure'] = $(this).find('failure').text().substr(0, $(this).find('failure').text().indexOf("/media/"));
                                }
                            }
                        });
                    }
                    else if($(this).attr('name').indexOf('Test') !== -1){
                        curr_class = $(this).attr('name').replace('Test', '');

                        //This hard code process for running test filter
                        if(jQuery.isEmptyObject(result)){
                            result[curr_allo] = {};
                        }

                        result[curr_allo][curr_class] = {
                            "attr" : {
                                "tests"      : $(this).attr('tests'),
                                "assertions" : $(this).attr('assertions'),
                                "failures"   : $(this).attr('failures'),
                                "errors"     : $(this).attr('errors'),
                                "time"       : process_time($(this).attr('time'))
                            }
                        };
                    }
                    else{
                        curr_allo = $(this).attr('name');
                        result[curr_allo] = {"attr" : {
                            "tests"      : $(this).attr('tests'),
                            "assertions" : $(this).attr('assertions'),
                            "failures"   : $(this).attr('failures'),
                            "errors"     : $(this).attr('errors'),
                            "time"       : process_time($(this).attr('time'))
                        }};

                    }
                });
                if(jQuery.isEmptyObject(result) === false){
                    //render data XML
                    show_result_test(result);

                    //event collapse and expand
                    $('.alloc, .class, .func, .test_case').click(function (event) {
                        var id = $(this).attr('id');
                        if(event.target.id !== id){
                            return;
                        }

                        if($(this).children().css('display') === 'none'){
                            $(this).removeClass('minus');
                            $(this).children().css('display', '-webkit-inline-box');
                            $('#attr_result').children().css('display', 'none');
                            $('#' + id + '_attr').css('display', 'initial');
                        }
                        else{
                            $(this).addClass('minus');
                            $(this).children().css('display', 'none');
                            $('#attr_result').children().css('display', 'none');
                        }
                    });
                }
            }
        });
    }

    $('#run_test').click(function () {
        $('#run_msg').append('Running test.....');
        var cmd = '';
        if($('#files').val()){
            var allocation    = $('#allocation').val();
            var widgets_child = $('#widgets_child').val();
            var class_name = $('#files').val().substr(0, $('#files').val().indexOf(' - '));
            var function_name = $('#files').val().substr($('#files').val().indexOf('- ') + 2, $('#files').val().length).replace(".json", "");

            allocation = (widgets_child && allocation === 'admin') ? (allocation + '/' + widgets_child) : allocation;
            cmd = function_name + ' ' + class_name + ' ' + '/media/sf_www/phpunit_pwater/tests/application/' + allocation + '/' + class_name + '.php';
        }

        $.ajax({
            url: 'common.php',
            data: {
                'run_test'  : true,
                'cmd'       : cmd
            },
            success: function () {
                $('#run_msg').empty();
                window.open('test_result.html');
            }
        });
    });

    $('#run_specific').click(function () {
        $('#specific_elements').toggle();
    });

    function process_class_and_func_name(str) {
        var flag_pos   = str.indexOf('-');
        var class_name = str.substr(0, flag_pos - 1);
        var func_name  = str.substr(flag_pos + 2, str.length);

        $('#class_name').val(class_name);
        $('#function_name').val(func_name);
    }

    function show_result_test(result) {
        if(jQuery.isEmptyObject(result)){
            return;
        }

        var first_proverty = '';
        //shift render first property if object has '""' property
        if(Object.keys(result)[0] === ''){
            first_proverty = result[''];
            delete result[''];
            var overall = '';
            var fail_cls = (first_proverty.attr.failures > 0) ? 'failure' : '';
            var err_cls = (first_proverty.attr.errors > 0) ? 'failure' : '';
            overall += "<div>Overall";
            overall += "<br>Tests: " + first_proverty.attr.tests + " cases.";
            overall += "<br>Assertions: " + first_proverty.attr.assertions + " times.";
            overall += "<br><span class='" + fail_cls + "'>Failures: " + first_proverty.attr.failures + " cases.</span>";
            overall += "<br><span class='" + err_cls + "'>Errors: " + first_proverty.attr.errors + " cases.</span>";
            overall += "<br>Time execute: " + first_proverty.attr.time;
            overall += "</div>";
            $('#overall').append(overall);
        }


        var str_result = '';
        var str_attr = '';
        $.each(result, function (key_alloc, value_alloc) {
            str_result += "<span id='" + key_alloc + "' class='alloc'>" + key_alloc;
            if(jQuery.isEmptyObject(value_alloc) === false){
                $.each(value_alloc, function (key_class, value_class) {
                    if(key_class !== 'attr'){
                        str_result += "<br><span id='" + key_class + "' class='class'>" + key_class;
                        if(jQuery.isEmptyObject(value_class) === false){
                            $.each(value_class, function (key_func, value_func) {
                                if(key_func !== 'attr') {
                                    str_result += "<br><span id='" + key_func + "' class='func minus'>" + key_func;
                                    if (jQuery.isEmptyObject(value_func) === false) {
                                        $.each(value_func, function (key_test_case, value_test_case) {
                                            if (key_test_case !== 'attr') {
                                                str_result += "<br><span id='" + key_test_case + "' class='test_case'>" + key_test_case;
                                                if (jQuery.isEmptyObject(value_test_case) === false) {
                                                    str_result += "<br><span class='param' >Assertions : " + value_test_case.assertions + "</span>";
                                                    str_result += "<span class='param'>Time : " + value_test_case.time + "</span>";
                                                    if ('failure' in value_test_case) {
                                                        str_result += "<br><span class='param failure'>Failure : " + value_test_case.failure + "</span>";
                                                    }
                                                }
                                                str_result += "</span>";
                                            }
                                            else{
                                                str_attr += create_attr_str(key_func, value_func, 'func');
                                            }
                                        });
                                    }
                                    str_result += "</span>";
                                }
                                else {
                                    str_attr += create_attr_str(key_class, value_class, 'class');
                                }
                            });
                        }
                        str_result += "</span>";
                    }
                    else{
                        str_attr += create_attr_str(key_alloc, value_alloc, 'alloc');
                    }
                });
            }
            str_result += "</span><br>";
        });
        $('#result').append(str_result);
        $('#attr_result').append(str_attr);
        change_color_parent_class();
        process_display_after_render();
    }

    function process_time(duration) {
        return Number(parseFloat(duration).toFixed(2)) + 's';
    }

    function create_attr_str(key, value, type) {
        var result = '';
        var fail_cls = (value.attr.failures > 0) ? 'failure' : '';
        var err_cls = (value.attr.errors > 0) ? 'failure' : '';
        result += "<div id='" + key +  "_attr' class='" + type +  "_attr'>";
        result += "<br><span class='title_attr'>" + key + "</span>";
        result += "<br>Tests: " + value.attr.tests + " cases.";
        result += "<br>Assertions: " + value.attr.assertions + " times.";
        result += "<br><span class='" + fail_cls + "'>Failures: " + value.attr.failures + " cases.</span>";
        result += "<br><span class='" + err_cls + "'>Errors: " + value.attr.errors + " cases.</span>";
        result += "<br>Time execute: " + value.attr.time;
        result += "</div>";
        return result;
    }
    
    function change_color_parent_class() {
        $('.param.failure').each(function () {
            $(this).parent().css('color', 'red');
            $(this).parent().parent().css('color', 'red');
            $(this).parent().parent().parent().css('color', 'red');
            $(this).parent().parent().parent().parent().css('color', 'red');
            $(this).parent().parent().parent().parent().parent().css('color', 'red');
        });
    }

    function process_display_after_render() {
        $('.func').children().css('display', 'none');
        $('.func').children().children().css('display', 'none');
        $('.func').children().children().css('display', 'none');
    }

    function get_dir(){
        //Get all folder in test_data folder
        $.ajax({
            url: 'common.php',
            data: {
                'get_dir'  : true
            },
            success: function (result) {
                $.each(result, function (key, val) {
                    $('#allocation').append("<option id='" + val +"'>" + val +"</option>");
                });
                setTimeout(get_files(result[Object.keys(result)[0]]), 1000);
            }
        });
    }

    function get_files(dir) {
        $.ajax({
            url: 'common.php',
            data: {
                'folder'  : dir
            },
            success: function (result) {
                if(dir !== 'widgets'){
                    $.each(result, function (key, value) {
                        if(dir.search('widgets') === -1){
                            $('#widgets_child').css('display', 'none');
                        }
                        $('#files').append("<option id='" + value +"'>" + value +"</option>");
                    });
                }
                else{
                    if($('#widgets_child').val()){
                        get_files(dir + '/' + $('#widgets_child').val());
                    }else{
                        get_widgets_child(dir);
                    }
                }
                if($('#files').val() && $("input[name='function_name']") > 0){
                    process_class_and_func_name($('#files').val());
                }
            }
        });
    }

    function get_widgets_child(dir) {
        $.ajax({
            url: 'common.php',
            data: {
                'folder' : dir
            },
            success: function (result) {
                $.each(result, function (key, value) {
                    $('#widgets_child').css('display', 'inline-block');
                    $('#widgets_child').append("<option id='" + value +"' class='widgets'>" + value +"</option>");
                });
                get_files(dir + '/' + result[Object.keys(result)[0]]);
            }
        });
    }
});