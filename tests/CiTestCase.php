<?php

if (!class_exists('Faker\Factory') || !class_exists('Mockery'))
{
    require_once __DIR__ . '/../vendor/autoload.php';
}

/**
 * Class Ci_Framework_TestCase
 * @author tuan_dung
 */
abstract class Ci_Framework_TestCase extends \PHPUnit_Framework_TestCase
{
    /** @var \MX_Controller */
    protected $CI;
    /** @var \Faker\Generator */
    protected $faker;
    /** @var bool */
    protected $debug = false;

    /** {@inheritdoc} */
    protected function setUp()
    {
        $this->faker = \Faker\Factory::create();
        $this->CI = &get_instance();
        $this->CI->db->cache_off();
        $this->CI->app_common->trans_start();
        foreach (glob(APPPATH . 'models/*.php', GLOB_NOSORT) as $v)
        {
            if (false !== stripos($v, 'order_20150401'))
            {
                continue;
            }
            $this->CI->load->model(basename($v, '.php'));
        }
        parent::setUp();
    }

    /** {@inheritdoc} */
    protected function tearDown()
    {
        \Mockery::close();

        if ($this->debug)
        {
            echo $this->CI->db->last_query();
            echo PHP_EOL . PHP_EOL;
        }
        $this->CI->app_common->trans_rollback();
        parent::tearDown();
    }

    /**
     * Get data to test in json file.
     *
     * @param string $allocation folder that contain test data
     * @return array|boolean
     * [CRE] - [tien_dat] - [2018-07-13]
     */
    protected function getDataProvider($allocation){
        $backtrace = debug_backtrace();

        $function = str_replace('_data_provider', '', $backtrace[1]['function']);
        $class = $backtrace[1]['class'];

        $file_path = '../phpunit_pwater/test_data/'.$allocation.'/'.$class.' - '.$function.'.json';
        try{
            if(!$file_content = file_get_contents($file_path)){
                throw new Exception("Can't get file.");
            }

            $result = json_decode($file_content, true);
            if(empty($result)){
                throw new Exception("File wrong or empty.");
            }
            return $result;
        }catch (Exception $e){
            echo $e->getMessage();
            return false;
        }
    }

}

/**
 * Class Model_TestCase
 * @author tuan_dung
 */
abstract class Model_TestCase extends \Ci_Framework_TestCase
{
    /** @var bool */
//    protected $debug = true;
    protected $allocation = 'models';
}